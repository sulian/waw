TERRAIN_PLAIN = 1
TERRAIN_MOUNTAIN = 2
TERRAIN_FOREST = 3
TERRAIN_SEA = 4

dbTerrains = {}

dbTerrains[TERRAIN_PLAIN] = {}
dbTerrains[TERRAIN_PLAIN].ID = "PLAIN"
dbTerrains[TERRAIN_PLAIN].Name = "Plain"
dbTerrains[TERRAIN_PLAIN].Img = love.graphics.newImage("img/map_plain.png")

dbTerrains[TERRAIN_MOUNTAIN] = {}
dbTerrains[TERRAIN_MOUNTAIN].ID = "MOUNTAIN"
dbTerrains[TERRAIN_MOUNTAIN].Name = "Mountain"
dbTerrains[TERRAIN_MOUNTAIN].Img = love.graphics.newImage("img/map_mountain.png")

dbTerrains[TERRAIN_FOREST] = {}
dbTerrains[TERRAIN_FOREST].ID = "FOREST"
dbTerrains[TERRAIN_FOREST].Name = "Forest"
dbTerrains[TERRAIN_FOREST].Img = love.graphics.newImage("img/map_forest.png")

dbTerrains[TERRAIN_SEA] = {}
dbTerrains[TERRAIN_SEA].ID = "SEA"
dbTerrains[TERRAIN_SEA].Name = "Sea"
-- dbTerrains[TERRAIN_SEA].Img = "img/map_.png"
