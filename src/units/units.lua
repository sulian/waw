dbUnits = {}

dbUnits["INFANTRY"] = {}
dbUnits["INFANTRY"].HP = 5
dbUnits["INFANTRY"].Range = 1
dbUnits["INFANTRY"].Gas = 50
dbUnits["INFANTRY"].Img = love.graphics.newImage("img/infantry.png")
dbUnits["INFANTRY"].TerrainEffects = {}
dbUnits["INFANTRY"].TerrainEffects[TERRAIN_PLAIN] = 1
dbUnits["INFANTRY"].TerrainEffects[TERRAIN_MOUNTAIN] = 2

dbUnits["TANK"] = {}
dbUnits["TANK"].HP = 10
dbUnits["TANK"].Range = 2
dbUnits["TANK"].Gas = 99
dbUnits["TANK"].Img = love.graphics.newImage("img/tank.png")
dbUnits["TANK"].TerrainEffects = {}
dbUnits["TANK"].TerrainEffects[TERRAIN_PLAIN] = 1
dbUnits["TANK"].TerrainEffects[TERRAIN_MOUNTAIN] = nil
